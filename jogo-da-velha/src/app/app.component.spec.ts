import { JogoDaVelhaService } from './jogo-da-velha/shared/jogo-da-velha.service';
import { JogoDaVelhaModule } from './jogo-da-velha';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        JogoDaVelhaModule
      ],
      providers: [JogoDaVelhaService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
