import { Component, OnInit } from "@angular/core";

import { JogoDaVelhaService } from "./shared";

@Component({
  selector: "app-jogo-da-velha",
  templateUrl: "./jogo-da-velha.component.html",
  styleUrls: ["./jogo-da-velha.component.css"]
})
export class JogoDaVelhaComponent implements OnInit {
  constructor(private jogoDaVelhaService: JogoDaVelhaService) {}

  ngOnInit() {
    this.jogoDaVelhaService.inicializar();
    console.log(this.jogoDaVelhaService.showInicio);
  }

  /** Retorna se o inicio da tela deve ser exibido.
   * @return boolean
   */

  get showInicio(): boolean {
    return this.jogoDaVelhaService.showInicio;
  }

  /** Retorna se o tabuleiro deve ser exibido
   * @return boolean
   */
  get showTabuleiro(): boolean {
    return this.jogoDaVelhaService.showTabuleiro;
  }

  /** Retorna se o final da tela deve ser exibido.
   * @return boolean
   */
  get showFinal() {
    return this.jogoDaVelhaService.showFinal;
  }

  /** Inicializa os dados de um nobo jogo.
   * @return void
   */
  iniciarJogo($event: any) {
    $event.preventDefault();
    this.jogoDaVelhaService.iniciarJogo();
  }

  /** Realiza uma jogada ao clicar no tabuleiro
   * @param posX number
   * @param posY number
   * @return void
   */
  jogar(posX: number, posY: number): void {
    return this.jogoDaVelhaService.jogar(posX, posY);
  }

  /** Retorna se a peça X deve ser exibida para a coordenada informada
   * @param posX number
   * @param posY number
   * @return boolean
   */
  exibirX(posX: number, posY: number): boolean {
    return this.jogoDaVelhaService.exibirX(posX, posY);
  }

  /** Retorna se a peça O deve ser exibida para a coordenada informada
   * @param posX number
   * @param posY number
   * @return boolean
   */
  exibirO(posX: number, posY: number): boolean {
    return this.jogoDaVelhaService.exibirO(posX, posY);
  }

  /** Retorna se a marcação de vitoria aparece para a coordenada informada
   * @param posX number
   * @param posY number
   * @return boolean
   */
  exibirVitoria(posX: number, posY: number): boolean {
    return this.jogoDaVelhaService.exibirVitoria(posX, posY);
  }

  /** Retorna o numero do jogador a jogar
   * @return number
   */
  get jogador(): number {
    return this.jogoDaVelhaService.jogador;
  }

  /** Inicia um novo jogo
   * @return void
   */
  novoJogo($event: any): void {
    $event.preventDefault();
    this.jogoDaVelhaService.novoJogo();
  }

}
