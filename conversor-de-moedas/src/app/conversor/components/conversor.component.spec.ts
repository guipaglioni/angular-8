import { FormsModule } from '@angular/forms';
import { ModalCotacaoComponent } from './../utils';
import { DataBrPipe } from '../pipes';
import { MoedaService, ConversorService } from '../services';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConversorComponent } from './conversor.component';
import { NumeroDirective } from '../directives';
describe('ConversorComponent', () => {
  let component: ConversorComponent;
  let fixture: ComponentFixture<ConversorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
         ConversorComponent,
         NumeroDirective,
         DataBrPipe,
         ModalCotacaoComponent
        ],
      providers: [
         MoedaService,
         ConversorService
         ],
         imports: [
           FormsModule,
         ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
