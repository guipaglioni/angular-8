import { TarefaRoutingComponent } from "./tarefas-routing.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EditarTarefaComponent } from "./editar";
import { ListarTarefaComponent } from "./listar";
import { CadastrarTarefaComponent } from "./cadastrar";
export const TarefaRoutes: Routes = [
  {
    path: "tarefas",
    component: TarefaRoutingComponent,
    children: [
      {
        path: "",
        component: ListarTarefaComponent
      },
      {
        path: "cadastrar",
        component: CadastrarTarefaComponent
      },
      {
        path: "editar/:id",
        component: EditarTarefaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(TarefaRoutes)],
  exports: [TarefaRoutingComponent],
  declarations: [TarefaRoutingComponent]
})
export class TarefasRoutingModule {}
