import { RouterModule, Routes } from '@angular/router';
import {  NgModule } from '@angular/core';

import { ConversorComponent } from './components';

export const ConversorRoutes: Routes = [
  {
    path: 'conversor',
    component: ConversorComponent,
    children: [
      {
        path: '',
        component: ConversorComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(ConversorRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConversorRoutingModule {}
