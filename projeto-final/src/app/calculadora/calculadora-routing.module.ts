import { RouterModule, Routes } from '@angular/router';
import {  NgModule } from '@angular/core';
import { CalculadoraComponent } from './components';


export const CalculadoraRoutes: Routes = [
  {
    path: 'calculadora',
    component: CalculadoraComponent,
    children: [
      {
        path: '',
        component: CalculadoraComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(CalculadoraRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CalculadoraRoutingModule {}
